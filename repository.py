
## Numpy not supported by mypy
#import numpy
#from numpy.linalg import norm

#def distance(a, b):
#    npA = numpy.array(a)
#    npB = numpy.array(b)
#    return norm(npA - npB)

from typing import *

import math

def distance(a: List[float], b: List[float]) -> float: 
    assert(len(a) == len(b))
    acc = 0.0
    for i in range(len(a)):
        acc += (a[i] - b[i])**2
    return math.sqrt(acc)

class Repository:
    def __init__(self):
        self.categories = {}
    
    def addCategory(self, name: str, vector: List[float]) -> None:
        self.categories[name] = vector
    
    def ranking(self, vector: List[float]) -> List[Tuple[str, float]]:
        ranking = [(name, distance(self.categories[name], vector)) for name in self.categories]
        ranking.sort(key = lambda tup: tup[1])
        return ranking
