
import unittest

from repository import Repository, distance

class TestRepository(unittest.TestCase):
    def testRepository(self):
        r = Repository()
        r.addCategory("a", [1.0])
        rank = r.ranking([1.0])
        self.assertEqual(len(rank), 1)
        self.assertEqual(rank[0][0], "a")
        self.assertEqual(rank[0][1], 0.0)
    
    def testDistance(self):
        self.assertEqual(distance([1], [1]), 0.0)
        self.assertEqual(distance([1], [0]), 1.0)

if __name__ == '__main__':
    unittest.main()

