
from typing import *

import collections

BORING = set(
    [" ", "\r", "\n", "\t"] + 
    ", . / [ ] ( ) ' \" „ “ ” | ; : - – — ^ ² ! ? …".split() + 
    [chr(i) for i in range(ord("0"), ord("9") + 1)])

class Counter:
    def __init__(self):
        self.total = 0
        self.counter = collections.Counter()
        self.allowed_list = [chr(i) for i in range(ord("a"), ord("z")+1)]
        self.allowed_set = set(self.allowed_list)
    
    def insertChar(self, c: str) -> None:
        if c not in self.allowed_set:
            if c not in BORING:
                print("Ignoring '{}' {}".format(c, ord(c)))
            return
        
        self.counter[c] += 1
        self.total += 1
    
    def toVector(self) -> List[float]:
        return [float(self.counter[i])/self.total for i in self.allowed_list]

def textToVector(s):
    c = Counter()
    for i in s:
        c.insertChar(i)
    return c.toVector()
