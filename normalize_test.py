
import unittest

from normalize import normalize, strip_accents


class TestNormalization(unittest.TestCase):
    def test_strip_accents(self):
        self.assertEqual(strip_accents("abc"), "abc")
        self.assertEqual(strip_accents("ěščřžýáíéř"), "escrzyaier")
    
    def testNormalize(self):
        self.assertEqual(normalize("AbC"), "abc")

if __name__ == '__main__':
    unittest.main()
