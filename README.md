Simple Language Classifier and a Mypy Demo
==========================================

Requires "mypy":

git clone https://github.com/JukkaL/mypy.git

Requires PYTHONPATH to be:

see source_pythonpath.sh

How to Test
------------

  ./test.sh

How to Check Types
-----------------

  ./check_types.sh

How to Run
----------

  python3 languages.py
