
import unittest

from counter import Counter

class TestCounter(unittest.TestCase):
    def testCounter(self):
        cnt = Counter()
        cnt.insertChar("a")
        v = cnt.toVector()
        self.assertEqual(v[0], 1)
        self.assertEqual(sum(v), 1)

if __name__ == '__main__':
    unittest.main()

