Ježíš Kristus
Tento článek pojednává o ústřední postavě křesťanství. O jméně, jeho dalších nositelích a významech pojednává článek Ježíš.
Ježíš Kristus
Různé podoby Ježíše Krista
Různé podoby Ježíše Krista
Narození	mezi 7 až 2 př. n. l.
Vexilloid of the Roman Empire.svg Judea, Římská říše
Úmrtí	30 až 33 n. l.
Vexilloid of the Roman Empire.svg Jeruzalém, Judea
Příčina úmrtí	ukřižování
Bydliště	Nazaret, Galilea
Povolání	řemeslník (tesař, možná též kameník)
Domovské město	Nazaret (dle NZ)
Znám jako	Ježíš Kristus
Ježíš z Nazareta
Ježíš Nazaretský
„Galilejský“
Ješua Mešiacha
Titul	mesiáš (mašiach)
Období	starověk, období vzniku římského císařství
Nábož. vyznání	antický judaismus
Rodiče	Maria (matka)
Josef (otec)
Některá data mohou pocházet z datové položky.
Ježíš Kristus (mezi 7 a 1 př. n. l.[1] – mezi 29 a 36 n. l.), známý také jako Ježíš Nazaretský či Ježíš z Nazareta, je ústřední postavou křesťanství.
Ježíš veřejně působil jako pocestný kazatel v oblasti dnešního Izraele a západního břehu Jordánu. Hlásal brzký příchod Božího království a vyzýval k obrácení či pokání. Kolem roku 30 jej Římané v Jeruzalémě ukřižovali. Křesťané považují Ježíše za Židy očekávaného Mesiáše, Spasitele lidstva a Božího Syna, muslimové za jednoho z nejvýznamnějších proroků.
Kritickým historickým bádáním došlo od 19. století k odlišení náboženské postavy Ježíše Krista a historické osoby Ježíše Nazaretského. Od nepřesně vypočítaného data Ježíšova narození se počítají léta našeho letopočtu.
Obsah  [skrýt] 
1 Jméno
2 Život podle historických pramenů
3 Život podle Nového zákona
3.1 Narození
3.1.1 Místo narození
3.1.2 Datum narození
3.2 Dětství
3.3 Křest Krista
3.4 Veřejné působení
3.5 Uzdravování
3.6 Učedníci a stoupenci
3.7 Odsouzení a smrt
3.8 Prázdný hrob
3.9 Nanebevstoupení
4 Ježíšovo učení
5 Ježíš v pojetí různých náboženství
5.1 Křesťanství
5.2 Judaismus
5.3 Islám
6 Odkazy
6.1 Poznámky
6.2 Reference
6.3 Literatura
6.3.1 Primární prameny
6.3.2 Sekundární literatura
6.4 Související články
6.5 Externí odkazy
Jméno[editovat | editovat zdroj]
Podrobnější informace naleznete v článcích Ježíš a Kristus.
Jméno Ježíš je česká podoba řeckého Ίησοῦς (Iésús), které vzniklo helenizací vlastního jména יְהוֹשֻׁע‎‎ (Jehošua), případně staženého tvaru יְשׁוּע‎‎ (Ješua), což je židovská forma jeho jména, spolu s přívlastkem ha-Nocri tj. Nazaretský. První část jména (יָה) je zkrácenou formou Božího jména „JHVH“, druhou část pak tvoří sloveso יָשַע‎‎ (jáša‘) „pomáhat“, „zachraňovat“, „spasit“. Celé jméno pak lze přeložit jako „záchrana přichází od Boha“. V přesném smyslu „Bůh je záchrana“. Přívlastek „Kristus“ je taktéž řeckého původu (χριστός christos) a je doslovným překladem hebrejského מָשִׁיחַ‎‎ (mašíach), jež znamená „pomazaný“, „mesiáš“, tedy židy očekávaný vykupitel Izraele.
Život podle historických pramenů[editovat | editovat zdroj]
Podrobnější informace naleznete v článku Historický Ježíš.
Pro zkoumání historické osobnosti Ježíše Nazaretského se využívá textové, historické a literární kritiky biblických a souvisejících textů. Kromě toho se zkoumají další prameny, jako mimokřesťanské texty pocházejí buď ze židovského (Flavius Iosephus, Talmud) nebo z římského prostředí (Plinius mladší, Tacitus, Suetonius)[2] a biblická archeologie. Zásadním problémem pro historické zkoumání Ježíšova života jsou klíčové písemné prameny (evangelia), které vznikly s časovým odstupem asi 30-70 let, s účelem šířit náboženské myšlenky, nikoli zachytit historii, a které nelze dostatečně konfrontovat s prameny jiného původu.
I když neexistuje spolehlivý fyzický důkaz o Ježíšově existenci, většina badatelů ji předpokládá. Zřejmě šlo o galilejského rabína a židovského náboženského reformátora, který se narodil krátce před začátkem letopočtu. Většina údajů o jeho životě však zůstává nejistá a hypotetická.
Život podle Nového zákona[editovat | editovat zdroj]

Ikona Ježíše Krista ze 6. století.
Křesťanský příběh Ježíše Krista popisuje Nový zákon v evangeliích, ve třech synoptických a poněkud odlišném Janově. Další informace podávají apokryfní evangelia, které však církve neuznávají za závaznou součást Bible.
Narození[editovat | editovat zdroj]
Podrobnější informace naleznete v článcích Narození Páně a Panenské početí Ježíše Krista.
Evangelia označují Ježíše jako „syna Josefova“,[3] „syna Josefova z Nazareta“[4] a jako „syna Mariina“.[5] Josef byl tesařem a nedochovalo se o něm mnoho dalších spolehlivých údajů, je možné, že se nedožil Ježíšova veřejného působení. O Ježíšově matce Marii se evangelia a Skutky apoštolů zmiňují ještě na dalších místech a po smrti svého syna patřila k raně křesťanské obci v Jeruzalémě.[6] Evangelia zmiňují také Ježíšovy bratry či bratrance Jakuba, Josefa, Šimona a Judu,[7] avšak vzhledem k širšímu vymezení slova ἀδελφός (adelfos) nelze s jistotou určit, zda Ježíš nějaké sourozence měl.
Markovo evangelium o Ježíšově dětství mlčí a Janovo evangelium před záznam o jeho vystoupení předřazuje pouze básnicko-teologický prolog, který s využitím řeckého filosofického pojmu λόγος (logos, slovo) líčí Ježíše jako odvěkou boží tvůrčí sílu, skrze niž vznikl celý svět.[8] Evangelium Matoušovo a Lukášovo, společně s některými pozdějšími apokryfními texty, věnují Ježíšovu narození a dětství značnou pozornost. I když některé jednotlivé údaje mohou být historicky věrné, má jejich líčení často spíše legendární charakter a někteří historikové[kdo?] poukazují i na to, že se v nejstarších vrstvách Nového zákona žádné informace tohoto druhu neobjevují.
Matoušovo a Lukášovo líčení Ježíšova narození však mělo bezesporu nesmírný vliv na formování křesťanské teologie a kultury. Hlavním smyslem těchto líčení Ježíšova narození je především poselství pro čtenáře evangelia, které chce předložit, kým Ježíš od počátku byl. Představují tedy ranou christologii zpracovanou pomocí vyprávění. Ježíšovy rodokmeny[9] mají Krista zapojit do linie významných náboženských a světských vůdců Izraele, příběh o početí z Ducha svatého a narození z panny[10] ukazuje Ježíšův božský původ. Vyprávění o útěku Ježíšovy rodiny do Egypta před záští krále Heroda Velikého[11] navazuje na pobyt izraelského národa v Egyptě, poukazuje na boží péči o Ježíše a umožňuje na dítě aplikovat staré biblické texty a přeznačit je na předpovědi událostí spojených s Ježíšem. Líčení kanonických evangelií však nelze zároveň zcela odmítnout jako historicky bezcenné.
Některé zprávy o Ježíšově dětství z nekanonických zdrojů (Pseudo-Tomášovo evangelium dětství) navíc zřejmě chtějí uspokojit i senzacechtivou zvědavost líčením spektakulárních zázraků údajně konaných chlapcem Ježíšem.
Místo narození[editovat | editovat zdroj]

Mistr vyšebrodského oltáře, Narození Krista, kolem roku 1350
Historická svědectví, která jsou k dispozici, neumožňují určit přesně datum Ježíšova narození a smrti. Podle Matoušova evangelia se Ježíš narodil v judském městečku Betlémě,[12] v čemž shledává naplnění starozákonního proroctví Micheášova.[13] Proto někteří historikové[kdo?] nepovažují tento údaj za zcela spolehlivý a domnívají se, že se může jednat o legendární údaj vytvořený na základě připodobnění Ježíše ke králi Davidovi, který pocházel právě z Betléma.[14]
Údaj, že Ježíš žil v galilejském Nazaretě, odkud dostává Ježíš také přídomek „Nazaretský“, případně „Nazorejský“.[15] je pak obecně považován za autentický, neboť v Ježíšově době nebyla s tímto místem spojována žádná očekávání.[16] Evangelium podle Matouše sice uvádí, že tento údaj byl proroky předpovězen, avšak starozákonní proroctví tento údaj neobsahují; snad se může jednat o další davidovský odkaz, když kniha Izajáš nazývá mesiáše „proutkem (נצר néser) z Jišajova pařezu.[17]
Datum narození[editovat | editovat zdroj]
Související informace naleznete také v článku Náš letopočet.
Biblická datace Ježíšova narození není jednoznačná: Evangelium podle Matouše klade Ježíšovo narození před smrt Heroda Velikého, k níž došlo roku 4 př. n. l.,[12] zatímco Evangelium podle Lukáše je klade do souvislosti s římským sčítání lidu, které se událo za působení syrského místodržitele P. Sulpicia Quirinia.[18] Doklady o censu hovoří o roce 6 n. l., avšak je možné, že se nejednalo o první sčítání lidu za Quiriniova místodržitelství. Další indicii může představovat betlémská hvězda,[19] snad neobvyklá konjunkce planet, již bylo možné v Palestině pozorovat roku 7 př. n. l. Porovnáním údajů o vystoupení Ježíšova předchůdce (Jana Křtitele v 15. roce vlády císaře Tiberia, tedy roku 28 n.l.)[20] a samotného Ježíše, jemuž tehdy bylo asi třicet let,[21] vychází rok Ježíšova narození někdy kolem přelomu letopočtu. Většina historiků dnes klade Ježíšovo narození mezi roky 7 a 4 př. n. l.
Rok Ježíšova narození se pokusil vypočítat jako první na základě historických záznamů, dostupných v 6. století, mnich Dionysius Exiguus, který však tento rok neurčil přesně. Na základě Dionysiova výpočtu se přesto začal užívat křesťanský letopočet, který je dnes letopočtem celosvětově nejběžněji používaným. Ke slovu přišel znovu asi před deseti lety, zvláště díky studiím Giorgia Fedalta, který využil výsledků pozorování washingtonské U. S. Naval Observatory a klade Ježíšovo narození do roku 1 před naším letopočtem.[zdroj?]
Tradiční datum oslavy Ježíšova narození, 25. prosinec (Vánoce), se ustanovuje ve 3. století v souvislosti s velikonočním cyklem (srov. původ Vánoc). Ve druhé polovině 20. století se však zejména mezi liturgisty ujala myšlenka, že 25. prosinec je pouze konvenční datum, které si zvolili římští křesťané, aby vytlačili kult nepřemožitelného Slunce, tzn. svátek Mitrův nebo císařův, který připadal na zimní slunovrat. Je pravdou, že po Konstantinově ediktu by církev mohla mít touhu zužitkovat nějaký svátek zanikajícího pohanství, nikoli však vymýšlet z ničeho nic datum tak zásadního významu. Navíc, kult nepřemožitelného Slunce zavedl císař Aurelianus roku 274, zatímco první zmínku o 25. prosinci jako dni Narození Krista nacházíme u Hippolyta Římského roku 204 (Komentář ke knize proroka Daniela). K tomu lze připojit také homilii Jana Zlatoústého z roku 386, kde tvrdí, že římská církev zná datum Kristova narození z archivu římské říše, ve kterém se uchovala akta o sčítání lidu provedeném na rozkaz císaře Augusta.
Dětství[editovat | editovat zdroj]
Pokud nepočítáme zmínky o narození tak jediná další zmínka o Ježíšově dětství v Bibli je příběh dvanáctiletého Ježíše v jeruzalémském chrámě v Lukášově evangeliu. Další, apokryfní, je zmíněné Pseudo-Tomášovo evangelium dětství.
Křest Krista[editovat | editovat zdroj]

Řeka Jordán

Andrea del Verrochio, Kristův křest, okolo roku 1475
Hlasatel pokání Jan Křtitel, který působil v poušti poblíž řeky Jordánu (podle Bible) a křtil zde kajícníky na znamení lítosti a odpuštění hříchů ,[22] je v Novém zákoně líčen a křesťanstvím chápán jako „předchůdce Páně“. Zda Ježíš sám patřil po nějakou dobu k jeho okruhu, není jisté; přes zjevné rozdíly mezi asketou Janem a Ježíšem přijímajícím pozvání na hostiny však sdíleli naléhavou, radikální etiku spojenou s vizemi blížícího se konce světa.[23] Setkání obou mužů vyvrcholilo tím, že se Ježíš nechal od Jana pokřtít jako další kajícníci. Údajně se tak stalo v lokalitě Qasr al-Jahúd.[24] O této události zpravují všechna čtyři kanonická evangelia a vykládají ji jako mesiášskou iniciaci Ježíše: podle jejích líčení se při křtu ozval z nebe hlas a označil Ježíše za vyvoleného božího syna[25]
Křest v Jordánu je historiky považován za věrohodně doložený, pro shodu všech čtyř evangelií a především proto, že pozdější křesťané neměli důvod vymýšlet si událost, která staví Jana do pozice autority vůči Ježíšovi.[26] Křest je zároveň Biblí líčen jako začátek Ježíšova veřejného vystoupení. Podle Janova evangelia Ježíšovi učedníci poté po nějakou dobu křtili v jeho přítomnosti a byl o ně větší zájem než o Jana.[27] Na vrcholu svého působení však se Ježíš vzdal pevného místa a začal vystupovat jako putující kazatel v Galileji a okolních územích.
Veřejné působení[editovat | editovat zdroj]

Čínská malba Ježíše s apoštoly
Ježíš strávil většinu svého života v Galileji pod vládou Heroda Antipy. Až někdy ve věku třiceti let začal kázat a působit jako thaumaturg a vykladač Zákona. Sám říkal, že se narodil proto, aby vydal svědectví pravdě (Jan 18, 37 (Kral, ČEP))
Mnohým jednáním Ježíš narušoval dobové a náboženské konvence. Porušoval některá židovská pravidla (uzdravování nebo sbírání klasů v sobotu). Navzdory tehdejší vysoké hodnotě rodinných vazeb vyjádřil vícekrát, že vztah s Bohem překonává i rodinné vazby: např. opuštění rodičů ve dvanácti letech v chrámu (Mt 12, 47–50 (Kral, ČEP)), oslovení matky při svatbě v Káni, reakce na slova „Hle, tvoje matka a tvoji bratři jsou venku a hledají tě.“ (Mk 3, 32 (Kral, ČEP)) a rovněž své učedníky vyzýval k opuštění všeho pro hlásání evangelia, narušoval pojetí výlučnosti Židů atd. Nábožensky motivované veřejné očištění chrámu byla jedním z důvodů jeho zadržení před ukřižováním.[28]
Pahýl	Tato část článku je příliš stručná nebo neobsahuje všechny důležité informace. Pomozte Wikipedii tím, že ji vhodně rozšíříte.
Uzdravování[editovat | editovat zdroj]
Podrobnější informace naleznete v článku Ježíšovy zázraky.
Zázraky nebyly (a v některých filosofických okruzích nejsou dodnes) vnímány jako porušování přírodních zákonů (takový pojem tehdy neexistoval), ale jako znamení Boží moci, prorockého vhledu do skutečnosti.
V Bibli je zaznamenáno mnoho případů zázračného uzdravení jak tělesného, tak i duševního. Jde o horečky, slepotu, malomocenství a jeho příznaky, navrácení života mrtvému a další. Zvláštním případem uzdravování bylo vyhánění zlých duchů, praktikované některými křesťanskými církvemi dodnes (viz exorcismus), kteří měli způsobovat jak tělesné tak duševní trápení a nemoci. Při uzdravení dochází v Bibli často též k obrácení dotyčného, popř. jeho vyznání.[29]
Podle biblických vyprávění Ježíš uzdravoval lidi, kteří k němu přišli, často pouhým dotykem či modlitbou. V Bibli se můžeme dočíst, že Ježíš uzdravoval chromé i slepé, na které narazil. Například v J 4, 43 (Kral, ČEP) uzdraví Ježíš dítě královského služebníka jenom že mu řekne na požádání, že jeho syn je živý a když se vrátí domů, nalezne svého syna živého.
Učedníci a stoupenci[editovat | editovat zdroj]
Podrobnější informace naleznete v článku Apoštol.
Od počátku Ježíšova veřejného působení se kolem něho formoval okruh přívrženců. Vedle běžných posluchačů, kteří v počtu stovek až tisíců docházeli na Ježíšova kázání, když byl nablízku, a poskytovali mu podporu a pohostinství, se vyhranila skupina nejbližších učedníků. Lukášovo evangelium zná širší okruh sedmdesáti či dvaasedmdesáti učedníků,[30] vyslaných Ježíšem jako hlasatelé božího království. Především však Ježíše následovalo na jeho cestách „Dvanáct“, skupina učedníků dnes často označovaná jako dvanáct apoštolů, v čele s Šimonem Petrem, Janem a Jakubem Starším.[pozn. 1] Podle biblického podání je Ježíš sám vybral a povolal, tedy vyzval je, aby opustili majetek, práci a rodinu (přinejmenším Petr byl ženat) a putovali s ním.
Vzhledem k dobovým zvyklostem neobvykle pozitivní byl Ježíšův vztah k ženám. Vedle matky Marie se v jeho blízkosti objevují Marie Magdalská, Lazarovy sestry Marie a Marta, matka Jakuba a Jana Salomé a mnoho dalších. Ženy ze svých prostředků Ježíše a jeho učedníky podporovaly a některé je dokonce doprovázely.Lk 8, 1 (Kral, ČEP) Ženy hrají klíčovou roli v mnoha novozákonních příbězích a Ježíš s nimi jednal se stejnou vážností jako s muži.
Odsouzení a smrt[editovat | editovat zdroj]
Podrobnější informace naleznete v článcích Pašije a Ukřižování Ježíše Krista.

El Greco, Kristus nesoucí kříž, 1580
Všechna kanonická evangelia se shodují na skutečnosti, že Ježíš byl popraven ukřižováním v Jeruzalémě při nebo před židovským svátkem Pesach. K popravě došlo za působení Pontia Piláta, prefekta judské provincie v letech 26 až 36. Vzhledem k tomuto zvláště potupnému způsobu smrti, který kanonická evangelia líčí, považují historikové Ježíšovo ukřižování za historicky zcela hodnověrné.[31] Svědectví je podepřeno též židovskými prameny (viz výše).
Markovo evangelium označuje za den jeho smrti začátek Pesachu, pátek 15. nisanu,[32] Janovo evangelium hovoří o odpoledni 14. dne měsíce nisanu, tedy předvečeru svátku.[33] Vzhledem k tomu, že je obtížné si představit Ježíšovu popravu právě během největší ze židovských slavností, zdá se, že je z historického hlediska Janovo vyprávění přesnější.[34] V těchto letech pak 14. nisan připadal na pátek 7. dubna 30 nebo 3. dubna 33. Je-li se možno spolehnout na údaj, že když Ježíši bylo na začátku jeho veřejného působení třicet let[21] a že toto působení trvalo tři roky,[pozn. 2] připadá jako nejpravděpodobnější datum Ježíšovy smrti 7. duben roku 30.[35]
V otázce, který z těchto důvodů pro obžalobu Ježíšovu a jeho odsouzení byl nejdůležitější, existují rozličné názory. V Ježíšově působení bylo vícero prvků, které mohly být pro část židovského obyvatelstva těžko stravitelné: vyhnání penězoměnců z jeruzalémského chrámu,[36] spory s farizeji a saduceji,[37] učení o Ježíšově těle coby pokrmu[38] a další. Hlásání božího království mohlo být také vnímáno jako politicky nebezpečné jak ze strany Římanů, tak židů. Je zřejmě nemožné posoudit, který z těchto prvků sehrál při Ježíšově odsouzení hlavní úlohu.
Prázdný hrob[editovat | editovat zdroj]
Podle všech evangelií bylo Ježíšovo tělo bezprostředně po smrti sňato z kříže, zabaleno do pláten a položeno do skalního hrobu, který patřil Josefu z Arimatie.[39] Přes rozdílnost vyprávění jednotlivých evangelií lze pak konstatovat, že některé ženy šly posléze navštívit Ježíšův hrob, ale jeho tělo tam již nenašly, a že církev od počátku tento prázdný hrob vysvětlovala Ježíšovým vzkříšením.[40] Židovská velerada vysvětlovala tuto skutečnost krádeží Ježíšova těla, kterou spáchali jeho učedníci, když římská hlídka u hrobu spala.[40]
Avšak ani církev nedokazovala Ježíšovo vzkříšení z faktu prázdného hrobu, nýbrž zjeveními Ježíše jeho učedníkům, kteří začínají hlásat, že Bůh Ježíše vzkřísil z mrtvých. Ač se objevily snahy vysvětlovat tato zjevení jako důsledek euforie či halucinace učedníků, případně jako podvod jejich či pisatelů evangelií a dalších novozákonních spisů, důvěryhodnost těchto teorií zpochybňuje vícero okolností, nejvíce pak fakt, že všichni apoštolové museli pro obhajobu svého tvrzení ustát za cenu vlastního života. V případě podvodu pak lze předpokládat, že by zprávy o Ježíšově vzkříšení byly přesvědčivější a harmoničtější: rozdíly a rozpory mezi jednotlivými prameny tak potvrzují společné historické jádro.[41]
Nanebevstoupení[editovat | editovat zdroj]
Podrobnější informace naleznete v článku Nanebevstoupení Páně.
Matoušovo a Lukášovo evangelium nekončí prázdným hrobem a tím, jak se učedníci a některé ženy setkaly se vzkříšeným Kristem. Poslední událostí, kterou popisují, je Ježíšovo nanebevstoupení. Ježíš tehdy vystoupil ke svému Otci a dal učedníkům takzvané velké poslání - jít do celého světa (počínajíc Jeruzalémem, Judskem a Samařím) a zvěstovat evangelium. Tím, kdo je v tom bude vést, bude Duch svatý, na jehož seslání měli počkat. To je však okamžik, kde již začínají dějiny církve.
Ježíšovo učení[editovat | editovat zdroj]

Mozaika Krista coby Pantokratora, kupole kostela Dafné, Athény, mezi 1090 a 1100
Třebaže se křesťanství odkazuje na Ježíšovo učení, důrazy se v něm přesunuly a rozvinuly v jiném kulturním kontextu. Některé myšlenkové a společenské proudy vzešlé z Ježíšova působení byly hlavním proudem křesťanství odmítnuty jako heretické a dnes obecně nejsou pokládány za typicky křesťanské. Rekonstruovat původní Ježíšovo učení, oproštěné od pozdějších kulturních nánosů a interpretací, je nesnadným úkolem; jeden ze způsobů tohoto bádání představuje historicko-kritická metoda četby spisů Nového zákona.
Ježíšovo učení navazuje na židovské tradice, které se snaží, v návaznosti na prorockou tradici, obnovit. Ježíš hlásá dobrou zvěst (εὐαγγέλιον euangelion, evangelium) o osvobození od jha formálních a rituálních náboženských požadavků a znovuobjevení a posílení jejich jednotících principů: autentičnosti, plnosti a neokázalosti v lásce k Bohu a k bližnímu. Toto osvobození je spojeno s pokáním, obrácením, změnou smýšlení. Požadavky jsou zaměřeny na odpovědnost a smýšlení jednotlivců, nikoliv na vzpouru vůči státnímu nebo náboženskému systému jako celku. Ježíš podporuje pokorné, upřímné, statečné, spravedlivé, utiskované, nemocné atd. a přitom hrozí pyšným, pokrytcům, lidem parazitujícím na náboženství k vlastnímu prospěchu. Klade důraz na milosrdenství, odpuštění, lásku k nepřátelům, sebekritičnost, odpovědnost za aktivní využití života (podobenství o hřivnách, Mt 25, 14–30 (Kral, ČEP)), ukazuje smysl sebezáporu, utrpení a snášení obtíží. Etické principy sounáležitosti a milosrdenství k bližním klade nad náboženskou, etnickou či rodinnou příslušností i nad planou servilitu vůči Bohu. Je kladen důraz na víru, která se projevuje nejen ovocem ducha, ale i zázraky a mocnými činy; současně v podobenstvích a kázáních podporoval racionální uvažování a rozeznávání „znamení doby“. Dle novozákonního podání Ježíš cílevědomě organizoval šíření svého učení příkladem i výkladem.
V období jeruzalémských událostí před Ježíšovým ukřižováním se objevuje i myšlenka výkupné hodnoty Ježíšova utrpení a smrti, zejména v Ježíšově proslovu nad chlebem při poslední večeři (moje tělo, které se láme/vydává za vás). Tato myšlenka byla později v rámci vývoje křesťanství propracována do ucelené teologie vykoupení, na niž navazuje i teologie svátostí.
Ježíšovo sebepojetí není z evangelií jednoznačně zřejmé, což se promítá i v teologických sporech raného křesťanství.
Pahýl	Tato část článku je příliš stručná nebo neobsahuje všechny důležité informace. Pomozte Wikipedii tím, že ji vhodně rozšíříte.
Ježíš v pojetí různých náboženství[editovat | editovat zdroj]
Křesťanství[editovat | editovat zdroj]

Lidová představa křesťanů o Ježíši Kristu se odráží i v dobových uměleckých dílech. Socha v kostele Sv. Jakuba v Brně
Ježíš Kristus je křesťany považován za Božího Syna, který se vtělil, stal se člověkem a žil mezi lidmi, nakonec byl zabit (ukřižován) a po třech dnech vstal z mrtvých, byl vzkříšen a tato zázračná událost přináší všem jeho následovníkům spásu. Jeho život a působení je středem křesťanské víry. Křesťané ho považují za zakladatele křesťanské víry (v religionistice se někdy můžeme setkat s přisuzováním založení křesťanství sv. Pavlovi).
Pro křesťany je Ježíš Kristus očekávaným Mesiášem, Spasitelem, Bohem, který lidem přinesl spásu. Je jediným prostředníkem mezi Bohem a lidmi, je vyvrcholením Božího zjevení. Veškeré dějiny spásy mají v něm svůj střed – o něm hovořili proroci a celý Starý zákon, k němu a k jeho království směřují dějiny. Řecky je někdy nazýván „Pantokrátór“, vševládný. Je též tím, kdo povede poslední soud.
Podle křesťanské tradice byl Ježíš Kristus i dokonalým člověkem, je vzorem pro jednání křesťana a základem jeho morálky. Jednání křesťana má odpovídat tomu, co Ježíš učil a jak jednal. Tento princip se latinsky označuje imitatio Christi (napodobování Krista) a je obdobou judaistického imitatio Dei (napodobování Boha). Víra v to, kým Ježíš skutečně byl, se vyvíjela velmi intenzivně a dlouhodobě. Hlavními etapami tohoto vývoje jsou ekumenické koncily, které formulovaly hlavní christologická dogmata křesťanské víry v Ježíše Krista. Pro pochopení křesťanské víry v Ježíše Krista je pravděpodobně nejvýznamnějším čtvrtý ekumenický koncil, jenž se roku 451 sešel v Chalkedonul (451) a jeho formulace „jedna osoba ve dvou přirozenostech (tj. božské a lidské), bez smíšení, beze změny, bez oddělení a bez rozloučení“. Ne všechny křesťanské církve však tento koncil přijímají.
Jako typologický předobraz Ježíše Krista ve Starém zákoně může být chápána jakákoliv z mesiánských postav (Mojžíš, král David, a další).[42]
Pahýl	Tato část článku je příliš stručná nebo neobsahuje všechny důležité informace. Pomozte Wikipedii tím, že ji vhodně rozšíříte.
Judaismus[editovat | editovat zdroj]

Hebrejský nápis ze 17. století kolem sochy Ukřižovaného na Karlově mostě[pozn. 3]
Ježíše líčí Nový zákon jako obřezaného Žida, znalého náboženského zákona, jejž lidé oslovují jako rabbiho, učitele Zákona. První generace křesťanů pocházela z velké části ze Židů a prvokřesťanské společenství bylo možné dlouho považovat za jednu z četných židovských sekt. I novozákonní knihy zaznamenávají, že se křesťané účastnili bohoslužebného života synagogy, se židy se stýkali a považovali se za následovníky judaismu. Díky existenci mnoha řecky hovořících židovských komunit ve Středomoří se nové náboženství mohlo rychle šířit. Brzy však mezi oběma skupinami dochází ke střetům a zřejmě v souvislosti s dvěma židovskými povstáními (66–70 a 133–135) dochází k odcizení obou skupin.[zdroj?] Další generace židokřesťanů zřejmě splývá s celou církví, tvořenou křesťany ze židovství a z pohanství, a ztrácí svou vyhraněně židovskou identitu. Ponětí o Ježíšově osobě z judaismu však prakticky úplně nevymizela. Pro židy, kteří se nestali křesťany, zůstává Ježíš heretikem, pokud o něm ovšem věděli. V Ježíšově době si však mezi židovským obyvatelstvem zřejmě získalo větší proslulost množství jiných reformátorů židovství. Z hlediska židovství byla osoba Ježíše problematická nikoli kvůli jeho učení, ale kvůli křesťanské víře, která označovala Ježíše za Boha.
Teprve od začátku 20. století se mnozí Židé začali o Ježíše, coby učitele Zákona, zajímat a začali jeho učení studovat v kontextu židovských dějin. Někteří z nich[43] nacházejí překvapivé shody mezi Ježíšovým učením a učením farizejů, které evangelia karikují jako pokrytce. Jiní autoři poukazují na podobnosti s židovskou sektou esejců. Poslední generace židovských teologů běžně vnímá Ježíše jako součást židovských dějin 1. století, ovšem neuznává jej jako syna Božího či Boha, ale jako pozoruhodného kritika náboženských omylů a morálních poklesků své doby.
Nejkritičtější postoj vůči kultu Ježíše z Nazaretu zaujímají od počátku ortodoxní židé, kteří Ježíše kvůli jeho vlastnímu tvrzení, že je synem Božím a Mesiášem, považují za heretika, nebo pomatence. Ve skutečnosti ale naprostá většina současných ultraortodoxních židů žije v uzavřených společenstvích, a otázky vztahu k Ježíši Kristu nepovažuje za důležité.
Islám[editovat | editovat zdroj]

Perská miniatura Ježíšova kázání na hoře
Ježíš (arab. يسوع Jasú' či عيسى Ísá) je pro muslimy jedním ze čtyř velkých proroků (Abrahám, Jákob, Mojžíš, Ježíš), kteří předcházeli poslednímu a největšímu poslu Božímu, Muhammadovi. Muslimové nevěří, že byl Ježíš ukřižován, ani že sňal hříchy světa, ani že by po své smrti byl vzkříšen. Stejně tak odmítají názor, že by mohl být božím synem, což považují za rouhání proti jedinosti a jedinečnosti boží.[44] Některé aspekty křesťanské christologie však muslimové uznávají, např. narození z panny. Muslimové rovněž věří, že se právě Ježíš před dnem soudu navrátí na zem, obnoví spravedlnost a porazí „falešného proroka“ (Másih ad-Dadždžál). Ježíš je předmětem úcty muslimů, stejně jako jeho matka Marie (arab. Marjam).
