
from typing import *

from repository import Repository
from counter import Counter, textToVector
from normalize import normalize
import os

DIR = "data"
LANGS = ["cs", "de", "en", "sk"]
FILES = ["prague.txt", "jesus.txt", "bratislava.txt"]

def loadFiles(callback: Callable[[str, str, str], None]) -> None:
    for lang in LANGS:
        for fileName in FILES:
            fullFileName = os.path.join(DIR, lang, fileName) 
            with open(fullFileName) as f:
                content = f.read()
                #print(fullFileName, len(content))
                callback(lang, fileName, content)

def recognize(text: str, lang: str, repo: Repository) -> None:
    ranking = repo.ranking(textToVector(normalize(text)))
    if ranking[0][0] == lang:
        print("OK", lang)
    else:
        print("FAIL")
        
    print(text)
    print(ranking)
    print()

def main() -> None:
    counters = {} # type: Dict[str, Counter]
    for i in LANGS:
        counters[i] = Counter()
    
    def callback(lang: str, fileName: str, content: str):
        normalizedContent = normalize(content)
        for i in normalizedContent:
            counters[lang].insertChar(i)    
    
    loadFiles(callback)
    
    repo = Repository()
    
    for pair in counters.items(): # "i" would conflict
        repo.addCategory(pair[0], pair[1].toVector())

    recognize("toto je nějaký český psaní", "cs", repo)
    recognize("this text is definitely english", "en", repo)
    recognize("Die Terrorgruppe Islamischer Staat (IS) hat in der syrischen Oasenstadt Palmyra ein berüchtigtes Gefängnis gesprengt, das als Symbol der Regierungsmacht des Assad-Regimes galt. Die Islamisten hätten einen Großteil der Haftanstalt mit", "de", repo)
    recognize("Od pátku bylo ve Středozemním moři při jedné z největších uprchlických vln letošního roku zachráněno přes pět tisíc běženců", "cs", repo)
    recognize("Európska agentúra Frontex, ktorá v Stredomorí koordinuje záchrannú misiu Triton, v nedeľu informovala, že od piatku bolo vo vodách Stredozemného mora zachránených viac ako päťtisíc imigrantov z Afriky a Blízkeho východu", "sk", repo)
    recognize("That world of the heaviest of industry is no longer prominent in Europe. Even the biggest shipyards that remain (in Romania, Poland and Germany) are minnows compared to the yards of Asia.", "en", repo)
    
if __name__ == "__main__":
    main()
                
